# README #

This is a script to help generate Rubik's Cube diagrams.

### Usage ###

```

    Usage: ./make_diag.pl [options] -o <out-file.png> <color-codes>

options:
    -h,--help                       this
    -o,--out    out-file.png        write image to out-file.png (default: out.png)
    -a,--arrow  AZ                  draw an arrow from F tile A to F tile Z

    <color-codes> is a 21-character string (no spaces!) organized like:

        FFFFFFFFFDDDRRRUUULLL

    the FFFFFFFFF colors are organized in row-major order from UFL to DFR.
    DDDRRRUUULLL are organized in counter-clockwise order around the F face
    starting at DLF.

    where each character must be one of:

        /[Yy]/   yellow
        /[Bb]/   blue
        /[Rr]/   red
        /[Ww]/   white
        /[Gg]/   green
        /[Oo]/   orange
        /[Xx]/   gray
         -       blank/unused

    For example, the white face of a solved cube might be:

        wwwwwwwwwbbbooogggrrr
```
