#!/usr/bin/perl
use warnings;
use strict;
use Data::Dumper;
use Storable qw/dclone/;
use GD::Simple;
use GD::Polygon;

#-----------------

my $TP_WIDTH    = 20;
my $SD_WIDTH    = 5;
my $PAD         = 5;
my $MARGIN      = 8;

my $FN_OUT_DFLT = "out.png";
my $FN_OUT      = $FN_OUT_DFLT;
    
my $ALGORITHM   = undef;

my $TYPE_DFLT   = "3d";
my $TYPE        = $TYPE_DFLT;
    
#-----------------

for (my $i=0; $i<=$#ARGV; $i++) {
    if ($ARGV[$i] =~ /^-h|--help$/) {
        usage();
    } elsif ($ARGV[$i] =~ /^-o|--out$/) {
        $FN_OUT = $ARGV[++$i];
    } elsif ($ARGV[$i] =~ /^-t|--type$/) {
        $TYPE = $ARGV[++$i];
    } elsif (!defined($ALGORITHM)) {
        $ALGORITHM = $ARGV[$i];
    } else {
        usage(sprintf("Unknown option '%s'", $ARGV[$i]));
    }
}

if (!defined($ALGORITHM)) {
    $ALGORITHM = "";
}

#--------------------------------

sub usage {
    my ($msg) = @_;

    if (defined($msg)) {
        print("$msg\n\n");
    }

    print <<EOM;

    Usage: $0 [options] -o <out-file.png> "R2 B2 R F R' B2 R F' R"

options:
    -h,--help                       this
    -o,--out    out-file.png        write image to out-file.png (default: out.png)
    -t,--type   <3d|flat|text>      type of image to create (default: $TYPE_DFLT)

EOM
}


#-----------------

sub new_cube {
    my %cube = ();
    $cube{"F"} = [ map { my $x = $_;  [ map { "R" } (0..2) ] } (0..2) ] ;
    $cube{"U"} = [ map { my $x = $_;  [ map { "W" } (0..2) ] } (0..2) ] ;
    $cube{"R"} = [ map { my $x = $_;  [ map { "B" } (0..2) ] } (0..2) ] ;
    $cube{"B"} = [ map { my $x = $_;  [ map { "O" } (0..2) ] } (0..2) ] ;
    $cube{"D"} = [ map { my $x = $_;  [ map { "Y" } (0..2) ] } (0..2) ] ;
    $cube{"L"} = [ map { my $x = $_;  [ map { "G" } (0..2) ] } (0..2) ] ;
    return dclone(\%cube);
}

#-----------------

sub do_algorithm {
    my ($algorithm, $in_cubep) = @_;
    my @moves = split(/\s/, $algorithm);

    my $cube = $in_cubep;
    foreach my $m (@moves) {
        $cube = do_move($m,$cube);
    }
    return $cube;
}

#-----------------

sub do_move {
    my ($move, $in_cubep) = @_;
    $move = lc($move);
    my %in_cube = %{ $in_cubep };

    # N2 === NN
    if ($move =~ /.2/) {
        my $out_cube = do_move(substr($move,0,1), 
                           do_move(substr($move,0,1), $in_cubep));
        return $out_cube;
    } 
    # N' == NNN
    if ($move =~ /.'/) {
        my $out_cube = do_move(substr($move,0,1), 
                           do_move(substr($move,0,1), 
                               do_move(substr($move,0,1), $in_cubep)));
        return $out_cube;
    }

    # x rotates whole cube in "R" direction
    if ($move eq "x") {
        my %out_cube = %{ dclone($in_cubep) }; # just get the shape right
        for (my $r=0; $r<3; $r++) {
            $out_cube{"U"}[$r] = dclone($in_cube{"F"}[  $r]);
            $out_cube{"F"}[$r] = dclone($in_cube{"D"}[  $r]);
            $out_cube{"D"}[$r] = [reverse(@{dclone($in_cube{"B"}[2-$r])})];
            $out_cube{"B"}[$r] = [reverse(@{dclone($in_cube{"U"}[2-$r])})];
        }
        for (my $r=0; $r<3; $r++) {
            for (my $c=0; $c<3; $c++) {
                $out_cube{"R"}[$r][$c] = $in_cube{"R"}[2-$c][$r  ];
                $out_cube{"L"}[$r][$c] = $in_cube{"L"}[  $c][2-$r];
            }
        }
        return \%out_cube;
    }

    # y rotates whole cube in "U" direction
    if ($move eq "y") {
        my %out_cube = %{ dclone($in_cubep) }; # just get the shape right
        for (my $r=0; $r<3; $r++) {
            $out_cube{"F"}[$r] = dclone($in_cube{"R"}[$r]);
            $out_cube{"R"}[$r] = dclone($in_cube{"B"}[$r]);
            $out_cube{"B"}[$r] = dclone($in_cube{"L"}[$r]);
            $out_cube{"L"}[$r] = dclone($in_cube{"F"}[$r]);
        }
        for (my $r=0; $r<3; $r++) {
            for (my $c=0; $c<3; $c++) {
                $out_cube{"U"}[$r][$c] = $in_cube{"U"}[2-$c][$r  ];
                $out_cube{"D"}[$r][$c] = $in_cube{"D"}[  $c][2-$r];
            }
        }
        return \%out_cube;
    }

    # z rotates whole cube in "F" direction
    if ($move eq "z") {
        my $out_cube = do_algorithm("x y x'", $in_cubep);
        return $out_cube;
    }

    # R
    if ($move eq "r") {
        my %out_cube = %{ dclone($in_cubep) }; # just get the shape right
        for (my $r=0; $r<3; $r++) {
            $out_cube{"F"}[$r  ][2] = $in_cube{"D"}[$r  ][2];
            $out_cube{"D"}[$r  ][2] = $in_cube{"B"}[2-$r][0];
            $out_cube{"B"}[2-$r][0] = $in_cube{"U"}[$r  ][2];
            $out_cube{"U"}[$r  ][2] = $in_cube{"F"}[$r  ][2];
        }
        for (my $r=0; $r<3; $r++) {
            for (my $c=0; $c<3; $c++) {
                $out_cube{"R"}[$r][$c] = $in_cube{"R"}[2-$c][$r  ];
            }
        }
        return \%out_cube;
    }

    if ($move eq "l") { return do_algorithm("y2 r y2", $in_cubep); }
    if ($move eq "u") { return do_algorithm("z r z'",  $in_cubep); }
    if ($move eq "d") { return do_algorithm("z' r z",  $in_cubep); }
    if ($move eq "b") { return do_algorithm("x' u x",  $in_cubep); }
    if ($move eq "f") { return do_algorithm("x u x'",  $in_cubep); }

    return $in_cubep;

}

#-----------------

sub char2color {
    my ($char) = @_;
    if    ($char =~ /[Yy]/) { return "yellow"    ; } 
    elsif ($char =~ /[Bb]/) { return "blue"      ; }
    elsif ($char =~ /[Rr]/) { return "red"       ; }
    elsif ($char =~ /[Ww]/) { return "ghostwhite"; }
    elsif ($char =~ /[Gg]/) { return "limeGreen" ; }
    elsif ($char =~ /[Oo]/) { return "orange"    ; }
    elsif ($char =~ /-/   ) { return undef       ; }
    else                    { return "gainsboro" ; }
}

#-----------------

sub make_flat_img {
    my ($fn_out, $cube) = @_;

    my $FACE_WIDTH = $TP_WIDTH * 3;

    my $img = GD::Simple->new($FACE_WIDTH * 4 + $PAD * 3 + $MARGIN * 2, $FACE_WIDTH * 3 + $PAD * 2 + $MARGIN * 2);

    $img->fgcolor("black");

    my %coords = (
        "U" => { "x" => $MARGIN +   $FACE_WIDTH +   $PAD , 
                 "y" => $MARGIN                          },
        "L" => { "x" => $MARGIN + 0*$FACE_WIDTH + 0*$PAD , 
                 "y" => $MARGIN +   $FACE_WIDTH +   $PAD },
        "F" => { "x" => $MARGIN + 1*$FACE_WIDTH + 1*$PAD , 
                 "y" => $MARGIN +   $FACE_WIDTH +   $PAD },
        "R" => { "x" => $MARGIN + 2*$FACE_WIDTH + 2*$PAD , 
                 "y" => $MARGIN +   $FACE_WIDTH +   $PAD },
        "B" => { "x" => $MARGIN + 3*$FACE_WIDTH + 3*$PAD , 
                 "y" => $MARGIN +   $FACE_WIDTH +   $PAD },
        "D" => { "x" => $MARGIN +   $FACE_WIDTH +   $PAD , 
                 "y" => $MARGIN + 2*$FACE_WIDTH + 2*$PAD }
    );

    foreach my $f (qw/F U R L D B/) {
        foreach my $r (0..2) {
            foreach my $c (0..2) {
                my $clr = $cube->{$f}[$r][$c];
                $img->bgcolor(char2color($clr));
                $img->rectangle($coords{$f}{"x"} + $TP_WIDTH*$c,
                                $coords{$f}{"y"} + $TP_WIDTH*$r,
                                $coords{$f}{"x"} + $TP_WIDTH*(1+$c),
                                $coords{$f}{"y"} + $TP_WIDTH*(1+$r));
            }
        }
    }

    my $fh = undef;
    open($fh, ">$fn_out") or die;
    binmode $fh;
    print $fh $img->png;
}

#-----------------

sub make_3d_img {
    my ($fn_out, $cube) = @_;

    my $FACE_WIDTH = $TP_WIDTH * 3;

    my $img = GD::Simple->new($FACE_WIDTH * 4 + $PAD * 3 + $MARGIN * 2, $FACE_WIDTH * 3 + $PAD * 2 + $MARGIN * 2 - $FACE_WIDTH/2);

    $img->fgcolor("black");

    my %coords = (
        "L" => { "x" => $MARGIN + 0*$FACE_WIDTH + 0*$PAD                 , 
                 "y" => $MARGIN +   $FACE_WIDTH +   $PAD - $FACE_WIDTH/2 },
        "F" => { "x" => $MARGIN + 1*$FACE_WIDTH + 1*$PAD                 , 
                 "y" => $MARGIN +   $FACE_WIDTH +   $PAD - $FACE_WIDTH/2 },
        "D" => { "x" => $MARGIN +   $FACE_WIDTH +   $PAD                 , 
                 "y" => $MARGIN + 2*$FACE_WIDTH + 2*$PAD - $FACE_WIDTH/2 },
        "B" => { "x" => $MARGIN + 3*$FACE_WIDTH + 3*$PAD                 , 
                 "y" => $MARGIN                 +   $PAD                 },
    );

    # F L D
    foreach my $f (qw/F L D B/) {
        foreach my $r (0..2) {
            foreach my $c (0..2) {
                my $clr = $cube->{$f}[$r][$c];
                $img->bgcolor(char2color($clr));
                $img->rectangle($coords{$f}{"x"} + $TP_WIDTH*$c,
                                $coords{$f}{"y"} + $TP_WIDTH*$r,
                                $coords{$f}{"x"} + $TP_WIDTH*(1+$c),
                                $coords{$f}{"y"} + $TP_WIDTH*(1+$r));
            }
        }
    }

    # U
    foreach my $r (0..2) {
        foreach my $c (0..2) {
            my $clr = $cube->{"U"}[$r][$c];
            $img->bgcolor(char2color($clr));

            #  01
            # 23
            
            my $x0 = $MARGIN + $FACE_WIDTH + $TP_WIDTH*$c + $TP_WIDTH*(2-$r) + $TP_WIDTH + $PAD;
            my $y0 = $MARGIN +             + $TP_WIDTH*$r - $TP_WIDTH*$r/2;

            my $x1 = $x0 + $TP_WIDTH;
            my $y1 = $y0;

            my $x2 = $x0 - $TP_WIDTH;
            my $y2 = $y0 + $TP_WIDTH/2;

            my $x3 = $x0;
            my $y3 = $y2;

            my $poly = new GD::Polygon;
            $poly->addPt($x0,$y0);
            $poly->addPt($x1,$y1);
            $poly->addPt($x3,$y3);
            $poly->addPt($x2,$y2);
            $img->polygon($poly);
        }
    }
            
    # R
    foreach my $r (0..2) {
        foreach my $c (0..2) {
            my $clr = $cube->{"R"}[$r][$c];
            $img->bgcolor(char2color($clr));

            #  1 
            # 03 
            # 2 
            
            my $x0 = $MARGIN + 2*$FACE_WIDTH + $TP_WIDTH*$c                + 2*$PAD;
            my $y0 = $MARGIN +   $FACE_WIDTH + $TP_WIDTH*$r - $TP_WIDTH*$c/2 + $PAD - $FACE_WIDTH/2;

            my $x1 = $x0 + $TP_WIDTH;
            my $y1 = $y0 - $TP_WIDTH/2;

            my $x2 = $x0;
            my $y2 = $y0 + $TP_WIDTH;

            my $x3 = $x1;
            my $y3 = $y0 + $TP_WIDTH/2;

            my $poly = new GD::Polygon;
            $poly->addPt($x0,$y0);
            $poly->addPt($x1,$y1);
            $poly->addPt($x3,$y3);
            $poly->addPt($x2,$y2);
            $img->polygon($poly);
        }
    }
            

    my $fh = undef;
    open($fh, ">$fn_out") or die;
    binmode $fh;
    print $fh $img->png;
}

#-----------------

sub show_cube {
    my ($cubep) = @_;

    for (my $r=0; $r<3; $r++) {
        print(" "x4);
        for (my $c=0; $c<3; $c++) {
            print($cubep->{"U"}[$r][$c]);
        }
        print("\n");
    }
    print("\n");

    for (my $r=0; $r<3; $r++) {
        foreach my $X (qw/L F R B/) {
            for (my $c=0; $c<3; $c++) {
                print($cubep->{$X}[$r][$c]);
            }
            print(" ");
        }
        print("\n");
    }
    print("\n");

    for (my $r=0; $r<3; $r++) {
        print(" "x4);
        for (my $c=0; $c<3; $c++) {
            print($cubep->{"D"}[$r][$c]);
        }
        print("\n");
    }

    print("\n");
    return $cubep;
}

#-----------------

#show_cube(do_algorithm("",new_cube()));
#show_cube(do_algorithm("z",new_cube()));
#show_cube(do_algorithm("u",new_cube()));
#my $cube = do_algorithm("r2 b2 r f r' b2 r f' r",new_cube());
my $cube = do_algorithm($ALGORITHM,new_cube());

if ($TYPE eq "3d") {
    make_3d_img($FN_OUT, $cube);
} elsif ($TYPE eq "flat") {
    make_flat_img($FN_OUT, $cube);
} elsif ($TYPE eq "text") {
    show_cube($cube);
} else {
    usage("Unsupported TYPE: '$TYPE'");
}
