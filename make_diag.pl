#!/usr/bin/perl
use warnings;
use strict;
use GD::Simple;
use Math::Trig;

my $CONFIG = "YYYYYYYYYGGGOOOBBBRRR";
my $FN_OUT = "out.png";
my @ARROWS = ();

my $TP_WIDTH = 20;
my $SD_WIDTH = 5;
my $PAD      = 5;
my $MARGIN   = 8;
    
#--------------------------------

for (my $i=0; $i<=$#ARGV; $i++) {
    if ($ARGV[$i] =~ /^-h|--help$/) {
        usage();
    } elsif ($ARGV[$i] =~ /^-o|--out$/) {
        $FN_OUT = $ARGV[++$i];
    } elsif ($ARGV[$i] =~ /^-a|--arrow$/) {
        push(@ARROWS, $ARGV[++$i]);
    } else {
        $CONFIG = $ARGV[$i];
    }
}

#--------------------------------

sub usage {
    print <<EOM;

    Usage: $0 [options] -o <out-file.png> <color-codes>

options:
    -h,--help                       this
    -o,--out    out-file.png        write image to out-file.png (default: out.png)
    -a,--arrow  AZ                  draw an arrow from F tile A to F tile Z

    <color-codes> is a 21-character string (no spaces!) organized like:

        FFFFFFFFFDDDRRRUUULLL

    the FFFFFFFFF colors are organized in row-major order from UFL to DFR.
    DDDRRRUUULLL are organized in counter-clockwise order around the F face
    starting at DLF.

    where each character must be one of:

        /[Yy]/   yellow
        /[Bb]/   blue
        /[Rr]/   red
        /[Ww]/   white
        /[Gg]/   green
        /[Oo]/   orange
        /[Xx]/   gray
         -       blank/unused

    For example, the white face of a solved cube might be:

        wwwwwwwwwbbbooogggrrr

EOM
}

#--------------------------------

sub char2color {
    my ($char) = @_;
    if    ($char =~ /[Yy]/) { return "yellow"    ; } 
    elsif ($char =~ /[Bb]/) { return "blue"      ; }
    elsif ($char =~ /[Rr]/) { return "red"       ; }
    elsif ($char =~ /[Ww]/) { return "ghostwhite"; }
    elsif ($char =~ /[Gg]/) { return "limeGreen" ; }
    elsif ($char =~ /[Oo]/) { return "orange"    ; }
    elsif ($char =~ /-/   ) { return undef       ; }
    else                    { return "gainsboro" ; }
}

#--------------------------------

sub make_topdown_img {
    my ($fn_out, $cfg) = @_;

    my $img = GD::Simple->new($TP_WIDTH*3+$SD_WIDTH*2+$PAD*2+$MARGIN*2,$TP_WIDTH*3+$SD_WIDTH*2+$PAD*2+$MARGIN*2);

    $img->fgcolor("black");

    my $cfg_U = substr($cfg, 0, 9);
    my $cfg_F = substr($cfg, 9, 3);
    my $cfg_R = substr($cfg,12, 3);
    my $cfg_B = substr($cfg,15, 3);
    my $cfg_L = substr($cfg,18, 3);

    # U
    foreach my $x (0..2) {
        foreach my $y (0..2) {
            my $c = substr($cfg_U,$y*3+$x,1);
            $img->bgcolor(char2color($c));
            $img->rectangle($MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH*$x,
                            $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH*$y,
                            $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH*($x+1),
                            $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH*($y+1)
                        );
        }
    }

    # B F
    foreach my $x (0..2) {
        my $c = substr($cfg_B,2-$x,1);
        if ($c eq "-") {
            next;
        }
        $img->bgcolor(char2color($c));
        $img->rectangle($MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH*$x,
                        $MARGIN                                  ,
                        $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH*($x+1),
                        $MARGIN + $SD_WIDTH                          
                    );
    }
    foreach my $x (0..2) {
        my $c = substr($cfg_F,$x,1);
        if ($c eq "-") {
            next
        }
        $img->bgcolor(char2color($c));
        $img->rectangle($MARGIN + $SD_WIDTH   + $PAD + $TP_WIDTH*$x,
                        $MARGIN + $SD_WIDTH   + $PAD + $TP_WIDTH*3 + $PAD,
                        $MARGIN + $SD_WIDTH   + $PAD + $TP_WIDTH*($x+1),
                        $MARGIN + $SD_WIDTH*2 + $PAD + $TP_WIDTH*3 + $PAD
                    );
    }

    # L R
    foreach my $y (0..2) {
        my $c = substr($cfg_L,$y,1);
        if ($c eq "-") {
            next;
        }
        $img->bgcolor(char2color($c));
        $img->rectangle($MARGIN                                      ,
                        $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH*$y    ,
                        $MARGIN + $SD_WIDTH                          ,
                        $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH*($y+1),
                    );
    }
    foreach my $y (0..2) {
        my $c = substr($cfg_R,2-$y,1);
        if ($c eq "-") {
            next;
        }
        $img->bgcolor(char2color($c));
        $img->rectangle($MARGIN + $SD_WIDTH   + $PAD + $TP_WIDTH*3 + $PAD,
                        $MARGIN + $SD_WIDTH   + $PAD + $TP_WIDTH*$y,
                        $MARGIN + $SD_WIDTH*2 + $PAD + $TP_WIDTH*3 + $PAD,
                        $MARGIN + $SD_WIDTH   + $PAD + $TP_WIDTH*($y+1)
                    );
    }

    foreach my $arrow (@ARROWS) {
        add_arrow($img, $arrow);
    }

    my $fh = undef;
    open($fh, ">$fn_out") or die;
    binmode $fh;
    print $fh $img->png;
}

#--------------------------------

sub add_arrow {
    my ($img,$arrow) = @_;
    my $sq_fr = substr($arrow,0,1);
    my $sq_to = substr($arrow,1,1);

    my $sq_fr_x_id = $sq_fr % 3;
    my $sq_fr_y_id = int($sq_fr / 3);

    my $sq_to_x_id = $sq_to % 3;
    my $sq_to_y_id = int($sq_to / 3);

    $img->fgcolor("black");
    my $x0 = $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH * $sq_fr_x_id + int($TP_WIDTH / 2);
    my $y0 = $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH * $sq_fr_y_id + int($TP_WIDTH / 2);
    my $x1 = $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH * $sq_to_x_id + int($TP_WIDTH / 2);
    my $y1 = $MARGIN + $SD_WIDTH + $PAD + $TP_WIDTH * $sq_to_y_id + int($TP_WIDTH / 2);

    $img->moveTo($x0,$y0);
    $img->lineTo($x1,$y1);

    my $FIN_LEN = 5;
    
    my $T       = atan2($y1-$y0,$x1-$x0);

    my $Tfin_hi = $T + pi/8;
    my $x2      = ($FIN_LEN * cos($Tfin_hi));
    my $y2      = ($FIN_LEN * sin($Tfin_hi));
    if (1) { $x2 = $x1 - $x2; } else { $x2 = $x1 + $x2; }
    if (1) { $y2 = $y1 - $y2; } else { $y2 = $y1 + $y2; }
    $img->moveTo($x1,$y1);
    $img->lineTo($x2,$y2);

    my $Tfin_lo = $T - pi/8;
    my $x3      = ($FIN_LEN * cos($Tfin_lo));
    my $y3      = ($FIN_LEN * sin($Tfin_lo));
    if (1) { $x3 = $x1 - $x3; } else { $x3 = $x1 + $x3; }
    if (1) { $y3 = $y1 - $y3; } else { $y3 = $y1 + $y3; }
    $img->moveTo($x1,$y1);
    $img->lineTo($x3,$y3);

}

#--------------------------------

make_topdown_img($FN_OUT, $CONFIG);
